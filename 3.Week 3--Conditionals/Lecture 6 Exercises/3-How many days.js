const monthNumber = 3;

if (monthNumber === 1) {
  console.log("January has 31 days.");
} else if (monthNumber === 2) {
  console.log("February has 28 days.");
} else if (monthNumber === 3) {
  console.log("March has 31 days.");
} else if (monthNumber === 4) {
  console.log("April has 30 days.");
} else if (monthNumber === 5) {
  console.log("May has 31 days.");
} else if (monthNumber === 6) {
  console.log("June has 30 days.");
} else if (monthNumber === 7) {
  console.log("July has 31 days.");
} else if (monthNumber === 8) {
  console.log("August has 31 days.");
} else if (monthNumber === 9) {
  console.log("September has 30 days.");
} else if (monthNumber === 10) {
  console.log("October has 31 days.");
} else if (monthNumber === 11) {
  console.log("November has 30 days.");
} else if (monthNumber === 12) {
  console.log("December has 31 days.");
} else {
  console.log("The month number should be between 1 and 12");
}
