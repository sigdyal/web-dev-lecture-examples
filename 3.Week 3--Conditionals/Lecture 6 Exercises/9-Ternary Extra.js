const computer1 = {
  time: 42,
  power: 600,
};
const computer2 = {
  time: 57,
  power: 400,
};

energyConsumption = {
  firstComp: computer1.power * computer1.time,
  secondComp: computer2.power * computer2.time,
};

console.log(`Energy consumed by Computer 1 is ${energyConsumption.firstComp}`);
console.log(`Energy consumed by Computer 2 is ${energyConsumption.secondComp}`);

lessPower = Math.min(energyConsumption.firstComp, energyConsumption.secondComp);

lessPower == energyConsumption.firstComp
  ? console.log("Therefore, Computer 1 is more efficient")
  : console.log("Therefore, Computer 2 is more efficient");
