const fruit1 = {
  name: "Pear",
  weight: 178,
};
const fruit2 = {
  name: "Lemon",
  weight: 120,
};
const fruit3 = {
  name: "Apple",
  weight: 90,
};
const fruit4 = {
  name: "Mango",
  weight: 150,
};

averageWeight =
  (fruit1.weight + fruit2.weight + fruit3.weight + fruit4.weight) / 4;
console.log(`The average weight of the fruits is ${averageWeight} grams`);

compareWeight = {
  pear: Math.abs(fruit1.weight - averageWeight),
  lemon: Math.abs(fruit2.weight - averageWeight),
  apple: Math.abs(fruit3.weight - averageWeight),
  mango: Math.abs(fruit4.weight - averageWeight),
};

closestWeight = Math.min(
  compareWeight.pear,
  compareWeight.lemon,
  compareWeight.apple,
  compareWeight.mango
);

switch (closestWeight) {
  case compareWeight.pear:
    console.log("Pear has a weight closest to the average weight.");
    break;
  case compareWeight.lemon:
    console.log("Lemon has a weight closest to the average weight");
    break;
  case compareWeight.apple:
    console.log("Apple has a weight closest to the average weight");
    break;
  case compareWeight.mango:
    console.log("Mango has a weight closest to the average weight");
    break;
}
