const name1 = "Dad";
const name2 = "Brother";
const name3 = "Sister";

length1 = name1.length;
length2 = name2.length;
length3 = name3.length;

if (length1 > length2 && length1 > length3 && length2 > length3) {
  console.log(`${name1}, ${name2}, ${name3}`);
} else if (length1 > length2 && length1 > length3 && length3 > length2) {
  console.log(`${name1}, ${name3}, ${name2}`);
} else if (length2 > length1 && length2 > length3 && length1 > length3) {
  console.log(`${name2}, ${name1}, ${name3}`);
} else if (length2 > length1 && length2 > length3 && length3 > length1) {
  console.log(`${name2}, ${name3}, ${name1}`);
} else if (length3 > length1 && length3 > length2 && length1 > length2) {
  console.log(`${name3}, ${name1}, ${name2}`);
} else {
  console.log(`${name3}, ${name2}, ${name1}`);
}
