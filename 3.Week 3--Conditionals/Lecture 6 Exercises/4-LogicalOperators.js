const score = 5;
const hoursPlayed = 5;
const price = 2;

hoursToPriceRatio = hoursPlayed / price;

if (
  (score >= 4 && price === 0) ||
  (score >= 4 && hoursToPriceRatio >= 4) ||
  (score === 5 && hoursToPriceRatio >= 2)
) {
  console.log("The game is worth the price.");
} else {
  console.log("The game is not worth playing.");
}
