const numberArray = [78, 32, 54];

if (numberArray[0] >= numberArray[1] && numberArray[0] >= numberArray[2]) {
  console.log(`${numberArray[0]} is the largest number`);
} else if (
  numberArray[1] >= numberArray[0] &&
  numberArray[1] >= numberArray[2]
) {
  console.log(`${numberArray[1]} is the largest number`);
} else if (
  numberArray[2] >= numberArray[0] &&
  numberArray[2] >= numberArray[1]
) {
  console.log(`${numberArray[2]} is the largest number`);
}

if (numberArray[0] <= numberArray[1] && numberArray[0] <= numberArray[2]) {
  console.log(`${numberArray[0]} is the smallest number`);
} else if (
  numberArray[1] <= numberArray[0] &&
  numberArray[1] <= numberArray[2]
) {
  console.log(`${numberArray[1]} is the smallest number`);
} else if (
  numberArray[2] <= numberArray[0] &&
  numberArray[2] <= numberArray[1]
) {
  console.log(`${numberArray[2]} is the smallest number`);
}

if ((numberArray[0] === numberArray[1]) & (numberArray[0] === numberArray[2])) {
  console.log("They are all equal.");
}
