const firstWord = "Apple";
const secondWord = "Apple";

if (firstWord.length > secondWord.length) {
  console.log(`${firstWord} is longer than ${secondWord}`);
} else if (firstWord.length < secondWord.length) {
  console.log(`${secondWord} is longer than ${firstWord}`);
} else {
  console.log(`${firstWord} and ${secondWord} are equal in length`);
}
