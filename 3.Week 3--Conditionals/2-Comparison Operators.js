const a = 3;
const b = 3;

name1 = "Matti Matti";
name2 = "Teppo";

if (name1.length > name2.length) {
  console.log("It works!");
}

if (name1.length === 11) {
  console.log("True");
}
if (name1.length !== 15) {
  console.log("True");
}
if (name1.length <= 11) {
  console.log("True");
}
if (a == 3 && name1.length === 11) {
  console.log("Multiple");
}
if (a == 3 && name1.length === 11) {
  console.log(`${name1} is exactly that.`);
}
