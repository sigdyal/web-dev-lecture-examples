const array1 = [55, 42, 38, 49, 88, 120, 37, 21, 45, 9];
const array2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
const array3 = [55, 42, 38, 49, 88, 120, 37, 21, 45, 9, NaN];

sum = 0;
diff = 0;

array1.forEach((number) => (sum += number));
console.log(sum);

array1.forEach((number) => (diff -= number));
console.log(diff);

fibbonacci = array2.reduce(
  (accumulator, currentValue) => accumulator + currentValue
);
console.log(fibbonacci);

product = array2.reduce(
  (accumulator, currentValue) => accumulator * currentValue
);
console.log(product);

difference = array2.reduce(
  (accumulator, currentValue) => currentValue - accumulator
);
console.log(difference);

difference2 = array3.reduce((accumulator, currentValue) => {
  if (isNaN(currentValue)) {
    currentValue = 0;
  }
  return currentValue - accumulator;
});
console.log(difference2);
