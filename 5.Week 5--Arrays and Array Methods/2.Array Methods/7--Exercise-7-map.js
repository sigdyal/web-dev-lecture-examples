/*
a) create a new array that contains the lengths of the animals’ names
b) create a new array that contains booleans that tell whether 
the specific animals have the letter ‘o’ as their second letter

Print the original array as well as both arrays created with map.
*/
const array = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

lengthOfAnimal = array.map((animal) => animal.length);

booleanArray = array.map((animal) => animal.charAt(1).includes("o"));

console.log("The array is ", array);
console.log("The length of each element is ", lengthOfAnimal);
console.log("The boolean value of each element is", booleanArray);
