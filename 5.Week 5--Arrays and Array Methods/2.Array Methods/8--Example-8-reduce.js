const ages = [13, 21, 33, 17, 46, null];
const sumOfAges = ages
  .map((value) => value * 2)
  .reduce((accumulator, value, index, arr) => {
    console.log(value, index);
    console.log(ages);
    console.log(arr);
    return accumulator + value;
  }, 5);
console.log(sumOfAges); // prints 130 (= 13 + 21 + 33 + 17 + 46)
