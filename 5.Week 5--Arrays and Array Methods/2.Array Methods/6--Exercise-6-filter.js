/*
Buiding on your solution for the a) part of the previous exercise, 
modify it so that instead of printing the whole array at once, 
you print the animals separately on their own lines.
*/

const array = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

animalsWithO = array.filter((animal) => animal.toLowerCase().includes("o"));

animalsWithO.forEach((animal) => {
  console.log(animal);
});

console.log("\n***********************************");

animalsWithNoHorO = array.filter(
  (animal) =>
    !animal.toLowerCase().includes("o") && !animal.toLowerCase().includes("h")
);

animalsWithNoHorO.forEach((animal) => {
  console.log(animal);
});
