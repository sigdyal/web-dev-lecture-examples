/*
Create a program that finds us all animals that
a) have the letter ‘o’ in their name.
b) don’t have a ‘h’ or ‘o’ in their name!

Print the array with the found animals.

*/

const array = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

animalsWithO = array.filter((animal) => animal.toLowerCase().includes("o"));
console.log(animalsWithO);

animalsWithNoHorO = array.filter(
  (animal) =>
    !animal.toLowerCase().includes("o") && !animal.toLowerCase().includes("h")
);
console.log(animalsWithNoHorO);
