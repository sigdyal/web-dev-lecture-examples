const numbers = [5, 13, 2, 10, 8];
let sum = 0;
let product = 1;

numbers.forEach((number) => (product *= number));
console.log(product);

numbers.forEach((number) => (sum += number));
console.log(sum / numbers.length);
