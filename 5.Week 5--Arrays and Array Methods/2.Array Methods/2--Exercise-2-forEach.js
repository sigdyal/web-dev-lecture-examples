const array = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];
const letter = "E";

for (let animal of array) {
  if (animal.toUpperCase().includes(letter)) {
    console.log(animal);
  }
}

console.log("\n");
