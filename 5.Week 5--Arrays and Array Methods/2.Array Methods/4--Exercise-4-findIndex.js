/*
Create a program that finds us the index of the first animal 
that has 6 or more letters in its name and prints the index.

Based on the found index, print the value as well.

*/

const array = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

const indexOfAnimal = array.findIndex((animal) => animal.length >= 6);

console.log(`The index is ${indexOfAnimal}`);
console.log(`The animal is ${array[indexOfAnimal]}`);
