/*
Create a program that finds us the first animal that ends in the letter ‘t’ and prints the animal.
*/

const array = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];

const firstAnimal = array.find((animal) => animal.endsWith("t"));
console.log(firstAnimal);

console.log("\n");

const secondAnimal = array.find(
  (animal) => animal.startsWith("d") && animal.endsWith("y")
);
console.log(secondAnimal);
