const sentence =
  "According to all known laws of aviation, there is no way a bee should be able to fly";

words = sentence.split(" ");
console.log(words);

for (const word of words) {
  if (word.toUpperCase().startsWith("A")) {
    console.log(word);
  }
}
