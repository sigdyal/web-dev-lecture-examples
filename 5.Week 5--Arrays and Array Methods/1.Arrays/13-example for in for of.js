const fruits = ["banana", "apple", "pear", "pineapple", "lemon"];

for (const i in fruits) {
  console.log(fruits[i]);
}

console.log(`\n`);

const fruits2 = ["banana", "apple", "pear", "pineapple", "lemon"];
for (const fruit of fruits) {
  console.log(fruit);
}
