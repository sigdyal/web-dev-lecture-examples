const fruits = ["banana", "apple", "lemon"];
console.log(fruits);

fruits.splice(2, 0, "pear"); // insert "pear" at index 2, remove 0 elements
console.log(fruits);

fruits.push("strawberry", "blueberry", "mulberry");
console.log(fruits);

fruits.splice(3, 2, "mango"); // insert "mango" at index 3 and remove 2 elements after that index
console.log(fruits);
