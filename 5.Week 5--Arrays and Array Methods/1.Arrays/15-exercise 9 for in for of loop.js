const fruits = [
  "cherry",
  "banana",
  "coconut",
  "apple",
  "pear",
  "pineapple",
  "lemon",
  "pumpkin",
];
const alsoFruits = [];

for (const fruit of fruits) {
  if (fruit.length > 6) {
    alsoFruits.push(fruit);
  }
}
console.log(alsoFruits);
