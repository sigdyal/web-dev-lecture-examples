const ages = [13, 21, 33, 17, 46, 55, 64, 88];

ages.push(23); // pushes an element to the last position.
console.log(ages);

ages.unshift(11); // pushes an element to the first position
console.log(ages);
