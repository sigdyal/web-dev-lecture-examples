const list = [5, 7, 2, 9, 3, 13, 15, 6, 17, 24];

for (const i in list) {
  if (list[i] % 3 === 0) {
    console.log(list[i]);
  }
}
console.log(`\n`);

for (const number of list) {
  if (number % 3 == 0) {
    console.log(number);
  }
}
