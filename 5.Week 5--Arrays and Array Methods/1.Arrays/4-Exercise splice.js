const array1 = ["the", "quick", "brown", "fox"];
array1[2] = "grey";
array1.push("over", "lazy", "dog");

console.log(array1);

array1.unshift("pangram:");

console.log(array1);

array1.splice(5, 0, "jump");
console.log(array1);

array1.splice(7, 0, "the");
console.log(array1);
