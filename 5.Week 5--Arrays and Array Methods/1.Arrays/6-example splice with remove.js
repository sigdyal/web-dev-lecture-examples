const ages = [13, 21, 33, 17, 46];
console.log(ages);
const removed = ages.splice(2, 1); // starting from index 2, remove 1 element

console.log(removed); // prints [ 33 ]
console.log(ages);
