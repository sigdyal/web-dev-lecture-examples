function sum(number) {
  if (number <= 0) {
    // stopping condition
    return 0;
  }
  console.log(number);
  return number + sum(number - 1); // calls itself
}

console.log(sum(5)); // prints 15 (= 5 + 4 + 3 + 2 + 1)
