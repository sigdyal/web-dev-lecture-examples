function sum(number) {
  console.log(number);
  return number + sum(number - 1); // calls itself
}

console.log(sum(0)); // prints 15 (= 5 + 4 + 3 + 2 + 1)
