class Rectangle {
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }
}

const rect1 = new Rectangle(3, 4);
const rect2 = new Rectangle(6, 8);
const rect3 = new Rectangle(5, 12);

console.log(rect1);
console.log(rect2);
console.log(rect3);
