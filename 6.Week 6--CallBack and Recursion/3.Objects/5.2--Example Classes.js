class Student {
  constructor(name, credits) {
    this.name = name;
    this.credits = credits;
  }

  addCredits(credits) {
    this.credits += credits;
  }
}

const student1 = new Student("Aili", 45);
student1.addCredits(10);
console.log(student1);
