/*


EXTRA: If the given fruit doesn’t exist, make the function print a list of supported fruits.

*/
//Create an object that contains fruits as keys and their average weights (in grams) as values.
//banana: 118, apple: 85, mango: 200, lemon: 65

const fruitObject = {
  banana: 118,
  apple: 85,
  mango: 200,
  lemon: 65,
};

//Then create a function printWeight(fruit) that prints the weight of the given fruit.
//For example, printWeight(“banana”) would print “banana weighs 118 grams”.

function printWeight(fruit) {
  if (fruit in fruitObject) {
    console.log(fruit, "weighs", fruitObject[fruit], "grams.");
  } else {
    console.log("The available fruits are " + Object.keys(fruitObject));
  }
}
printWeight("banana");
printWeight("pineapple");
