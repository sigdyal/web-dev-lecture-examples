class Rectangle {
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }
  getArea(width, height) {
    this.area = width * height;
  }
}
const rect1 = new Rectangle(3, 4);
const rect2 = new Rectangle(6, 8);
const rect3 = new Rectangle(5, 12);
rect1.getArea(3, 4);
rect2.getArea(6, 8);
rect3.getArea(5, 12);

console.log(rect1);
console.log(rect2);
console.log(rect3);
