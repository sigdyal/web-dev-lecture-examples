const student = {
  name: "Aili",
  credits: 45,
  courseGrades: {
    "Intro to Programming": 4,
    "JavaScript Basics": 3,
    "Functional Programming": 5,
  },
};

console.log(student.courseGrades); // prints object
console.log(Object.values(student.courseGrades)); // prints array of values
console.log(Object.keys(student.courseGrades)); // prints array of keys

console.log(student.courseGrades["JavaScript Basics"]); // prints value of 'JavaScript Basics' key

const courseName = "JavaScript Basics";
console.log(student.courseGrades[courseName]); //prints value of 'JavaScript Basics' key
