const testObj = {
  key: "value",
};

console.log(testObj);
console.log(testObj.key);

const student = {
  name: "Alice",
  attendingToSemester: true,
  credits: 40,
  id: 147181,
};

const studentArr = ["Alice", true, 40];
console.log(studentArr[0] + " has " + studentArr[2] + " credits.");

console.log(student.name + " has " + student.credits + " credits.");

student.attendingToSemester = false;
student.birthYear = 1990;
console.log(student);

const keyWeWantToChange = "attendingToSemester";
student[keyWeWantToChange] = true;
console.log(student);
