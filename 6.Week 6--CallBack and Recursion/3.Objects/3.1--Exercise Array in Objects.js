const student = {
  name: "Aili",
  credits: 45,
  courseGrades: {
    "Intro to Programming": 4,
    "JavaScript Basics": 3,
    "Functional Programming": 5,
  },
};

/*
Transform the student.courseGrades object into an array student.courses where each course is stored as an object
        {
            name: "Intro to Programming",
            grade: 3
        },
*/
const objectWeWantToChange = "courseGrades";
student[objectWeWantToChange] = [
  {
    name: "Intro to Programming",
    grade: 4,
  },
  {
    name: "JavaScript Basics",
    grade: 3,
  },
  {
    name: "Functional Programming",
    grade: 5,
  },
];
console.log(student); // Updated Student Object printed

/*
Print to console “Aili got 3 from Intro to Programming” 
by referring to the course data inside the courses array.
Hint: .find method can be useful!
*/

const ailiGrades = student.courseGrades.find((grade) =>
  Object.values(student.courseGrades)
);
console.log(ailiGrades);
console.log(student.name, "got", ailiGrades.grade, "from", ailiGrades.name);

/*
Now, create a new function to add a new course to the array: 
addCourse(courseName, courseGrade)
*/
function addCourse(courseName, courseGrade) {
  const newCourse = {
    name: courseName,
    grade: courseGrade,
  };
  student.courseGrades.push(newCourse);
}
addCourse("Basics of C++", 4);
console.log(student);
