//Create a similar student object as on the previous slide.
const student = {
  name: "Aili",
  credits: 45,
  courseGrades: {
    "Intro to Programming": 4,
    "JavaScript Basics": 3,
    "Functional Programming": 5,
  },
};

//-Print the student.
console.log(student);

/*Programmatically (without altering the original student object), 
  add a new course “Program Design” with a grade of 3 to the student.*/
const newCourse = "Program Design";
student.courseGrades[newCourse] = 3;

//Print the student again.
console.log(student);

//Programmatically, change the grade of the “JavaScript Basics” course to 4.
const changeGrade = "JavaScript Basics";
student.courseGrades[changeGrade] = 4;

//Print the student once more.
console.log(student);
