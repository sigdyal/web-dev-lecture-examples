const helloLookupObject = {
  en: "Hello World",
  fi: "Hei maailma",
  de: "Hallo Welt",
};

function helloWorld(language) {
  if (language in helloLookupObject) {
    console.log(helloLookupObject[language] + "!");
  } else {
    console.log(
      "Unknown language given! Supported languages are: " +
        Object.keys(helloLookupObject)
    );
  }
}

console.log(Object.keys(helloLookupObject)); // prints an array ['en', 'fi', 'de']
helloWorld("en"); // prints "Hello World!"
helloWorld("es"); // prints "Unknown language given! Supported languages are: en, fi, de "
