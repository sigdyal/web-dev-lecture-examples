function myCalculator(num1, num2, myCallback) {
  let sum = num1 + num2;
  myCallback(sum);
}

//Callback function myDisplayer which is passed to function myCalculator

function myDisplayer(numb) {
  console.log("Numbers:", numb);
}

myCalculator(5, 5, myDisplayer);
