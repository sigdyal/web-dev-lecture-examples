/*
Create a program that finds out the smallest number that
has a factorial that is divisible by 600.

Print the number.

Do not pre-calculate the answer, but use an infinite loop to
find the number and use the break statement to stop the
loop once the number has been found.
*/

let factorial = 1;
let number = 0;
while (true) {
  number++;
  factorial = factorial * number;
  if (factorial % 600 == 0) {
    break;
  }
}
console.log(number);
console.log(factorial);
