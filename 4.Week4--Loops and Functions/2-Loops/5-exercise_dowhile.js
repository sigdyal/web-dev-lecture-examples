let i = 0;
let text = "";
const limit = 5;

do {
  i++;
  const delimiter = i == limit ? "" : ",";
  text += i * 3 + delimiter;
} while (i < limit);
console.log(text);

//Another way

//Another way;

let index = 0;
let sum = 0;

do {
  index++;
  sum += 3;
  console.log(sum);
} while (index < 5);
