// 1.
let num1 = 0;
for (let index1 = 0; index1 < 11; index1++) {
  num1 = index1;
  console.log(num1 * 100);
}
console.log("*********************");

//2.
let num2 = 2;
for (let index2 = 0; index2 < 6; index2++) {
  num2 = 2 ** index2;
  console.log(num2);
}
console.log("*********************");

//3
let num3 = 3;
for (let index3 = 1; index3 < 6; index3++) {
  num3 = 3 * index3;
  console.log(num3);
}
console.log("*********************");

//4
let num4 = 10;
for (let index4 = 1; index4 <= 10; index4++) {
  num4 = 10 - index4;
  console.log(num4);
}
console.log("*********************");

//5
