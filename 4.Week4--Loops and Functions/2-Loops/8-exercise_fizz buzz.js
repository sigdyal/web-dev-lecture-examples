//for loop
let number;
for (let number = 1; number <= 100; number++) {
  if (number % 3 == 0 && number % 5 == 0) {
    console.log(number, "Fizzbuzz");
  } else if (number % 3 == 0 && number % 5 != 0) {
    console.log(number, "Fizz");
  } else if (number % 3 != 0 && number % 5 == 0) {
    console.log(number, "Buzz");
  } else {
    console.log(number);
  }
}
console.log("*************************************************");
//while loop
let whileNumber = 0;
while (whileNumber < 100) {
  whileNumber++;
  if (whileNumber % 3 == 0 && whileNumber % 5 == 0) {
    console.log(whileNumber, "Fizzbuzz");
  } else if (whileNumber % 3 == 0 && whileNumber % 5 != 0) {
    console.log(whileNumber, "Fizz");
  } else if (whileNumber % 3 != 0 && whileNumber % 5 == 0) {
    console.log(whileNumber, "Buzz");
  } else {
    console.log(whileNumber);
  }
}
