let index = 0;
let sum = 0;

while (index < 5) {
  index++;
  sum += 3;
  console.log(sum);
}
//A way to generate 3,6,9,12,15

let i = 1;
let text = "";
const limit = 5;

while (i <= limit) {
  const delimiter = i == limit ? "" : ",";
  text += i * 3 + delimiter;
  i++;
}
console.log(text);
