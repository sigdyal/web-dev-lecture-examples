let factorial = 1;
let number = 0;

while (number < 10) {
  number++;
  if (number % 3 == 0) {
    continue;
  }
  console.log(number);
  factorial *= number;
}
console.log(factorial);
