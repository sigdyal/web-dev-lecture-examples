/*
Create a function named exponentValueList that takes a number n as a parameter.
The number should print 2 multiplied by itself from 1... to n times, each value on its own line.
*/

function exponentValueList(n) {
  let number = 1;
  while (number <= n) {
    power = 2 ** number;
    console.log(power);
    number += 1;
  }
  if (n <= 0) {
    console.log("The number 'n' needs to be positive.");
  }
}
exponentValueList(4);
