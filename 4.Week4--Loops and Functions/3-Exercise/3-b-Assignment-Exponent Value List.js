/*
Add a parameter to exponentValueList so that instead of the number to raise in power 
always being 2, it can also be defined by the caller.
*/

function exponentValueList(n, base) {
  let number = 1;
  while (number <= n) {
    power = base ** number;
    console.log(`The ${base} raised to the power of ${number} is ${power}`);
    number += 1;
  }
  if (n <= 0) {
    console.log("The number 'n' needs to be positive.");
  }
}
exponentValueList(4, 3);
