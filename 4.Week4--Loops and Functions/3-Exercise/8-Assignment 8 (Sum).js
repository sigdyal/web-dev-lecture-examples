/*
Create a program that counts a sum like 1 + 2 + 3... 
until it exceeds 10000 and prints the last number that was added to the sum.
*/
let sum = 0;
let number = 0;
while (true) {
  number++;
  sum += number;
  if (sum >= 10000) {
    break;
  }
}
console.log(
  `The sum of the numbers till it exceeds 10000 is ${sum} and the last digit added is ${number}`
);
