/*
Create a function named minimum that takes 3 numbers as parameters.
The function should find the smallest of the given numbers and return it.
Call your function with different numbers as parameters to make sure it works. 
Forward the returned value to console.log to print the return value of your function.
DO NOT call console.log in the function itself, but instead call it after calling your function.
*/

function minimum(num1, num2, num3) {
  if (num1 < num2 && num1 < num3) {
    return num1;
  } else if (num2 < num1 && num2 < num3) {
    return num2;
  } else if (num3 < num1 && num3 < num1) {
    return num3;
  }
}
smallest = minimum(9, 8, 7);
console.log(`The minimum is ${smallest}`);
