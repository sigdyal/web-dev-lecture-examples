/*
Create a program where we have variables x and y coordinates representing the position of a robot, 
and a command string that tells the robot where it should move.
*/

const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE";
let x = 0;
let y = 0;

for (let i = 0; i <= commandList.length; i++) {
  if (commandList.charAt(i).toUpperCase() === "N") {
    y++;
  }
  if (commandList.charAt(i).toUpperCase() === "S") {
    y--;
  }
  if (commandList.charAt(i).toUpperCase() === "E") {
    x++;
  }
  if (commandList.charAt(i).toUpperCase() === "W") {
    x--;
  }
  if (commandList.charAt(i).toUpperCase() === "C") {
    i++;
  }
  if (commandList.charAt(i).toUpperCase() === "B{") {
    break;
  }
}
console.log(`Position of x is ${x}`);
console.log(`Position of y is ${y}`);
