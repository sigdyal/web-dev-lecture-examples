/*
You are holding a class that has students with ages of 20, 35, 27 and 44, 24 and 32.
a)
Create an array with the ages. Print the array.
*/
const ages = [20, 35, 27, 44, 24, 32];
console.log(ages);

/*
b)
Calculate and print the average age of the students.
Use a loop when calculating the sum of the ages.
*/
sumOfAges = 0;
for (let index = 0; index < ages.length; index++) {
  sumOfAges += ages[index];
}
averageAge = sumOfAges / ages.length;
console.log(`The average age of the students is ${averageAge}`);
