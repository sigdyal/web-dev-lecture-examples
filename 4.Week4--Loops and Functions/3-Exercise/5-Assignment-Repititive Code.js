/*
The following code has 3 triangles with different lengths for their cathetuses. 
The trianges' areas are calculated and printed.
However, if you look at the code, you'll notice that it has some repetition. 
In particular, the way the area is calculated for a triangle is repeated 3 times.
The code would be cleaner if that calculation was split into its own function that 
took a triangle as a parameter and returned the area of the given triangle. 
That function could  then be called 3 times (once for each triangle) instead of having 
the calculation just copy-pasted in the code.
Split the area calculation into its own function as described above and replace 
the repetitive calculations with calls to your function.
*/

const firstTriangle = { width: 7.0, length: 3.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 5.5, length: 5.0 };

function areaOfTriangle(width, length) {
  return (width * length) / 2;
}

first = areaOfTriangle(firstTriangle.width, firstTriangle.length);
second = areaOfTriangle(secondTriangle.width, secondTriangle.length);
third = areaOfTriangle(thirdTriangle.width, thirdTriangle.length);

console.log(`Area of first triangle: ${first}`);
console.log(`Area of second triangle: ${second}`);
console.log(`Area of third triangle: ${third}`);

function isBiggest() {
  if (first > second && first > third) {
    console.log("First is the largest");
  } else if (second > first && second > third) {
    console.log("Second is the largest");
  } else if (third > first && third > second) {
    console.log("Third is the largest");
  }
}
isBiggest();
