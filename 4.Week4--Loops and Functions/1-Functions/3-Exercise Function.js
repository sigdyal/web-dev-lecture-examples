//named function
function multiply(num1, num2) {
  return num1 * num2;
}
console.log(multiply(4, 6));

//Anonymous function
let product = function (num1, num2) {
  return num1 * num2;
};
console.log(product(7, 9));

//Arrow function
let alsoProduct = (num1, num2) => {
  return num1 * num2;
};
console.log(alsoProduct(12, 6));

let anotherProduct = (num1, num2) => num1 * num2;
console.log(anotherProduct(8, 15));
