const array1 = [11, 22, 33];

function changeArray(elmt) {
  elmt[1] = 100;
}
console.log(array1);
changeArray(array1);
console.log(array1);

const object1 = {
  firstName: "John",
  lastName: "Smith",
  age: 25,
};

function changeObject(names) {
  names.firstName = "Alice";
}
console.log(object1);
changeObject(object1);
console.log(object1);
