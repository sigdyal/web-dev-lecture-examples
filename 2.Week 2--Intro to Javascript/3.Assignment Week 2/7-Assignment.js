/*Assignment 7 (Area of a rectangle)
Create two variables: width and length.
Like in the last task, calculate and print out the area. However, instead of a square, 
we're now dealing with a rectangle.
Each variable represents the length of two opposing sides of the rectangle.
EXTRA: In addition to a rectangle, calculate the area of a triangle, 
with the variables representing the length of the triangle's cathetuses.
*/

const width = 5;
const height = 10;

AreaOfRectangle = width * height;
AreaOfTriangle = AreaOfRectangle / 2;

console.log(
  "The Area of the Rectangle with width",
  width,
  "and height",
  height,
  "is:",
  AreaOfRectangle
);
console.log(
  "The Area of the Triangle with width",
  width,
  "and height",
  height,
  "is:",
  AreaOfTriangle
);
