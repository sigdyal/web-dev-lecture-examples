/*
You're creating a game where players have income that is given to them as money at specific intervals.
Mid-way through the game's development, you realize that a player having more income than someone else 
gives them an unfairly large advantage. To reduce this advantage, you decide to expnonent each player's 
income with a number between 0 and 1 (say, 0.9) to reduce the effect larger income has on the player's 
final money. You could think of it as progressive taxing.
Create variables for the income of two different players. Give them values that are different from each other.
*/

const incomeA = 5000;
const incomeB = 9000;

IncomeDifferece = Math.max(incomeA, incomeB) - Math.min(incomeA, incomeB);

console.log("When the income is", incomeA, "and", incomeB);
console.log("The income difference before exponentiation =", IncomeDifferece);

exponetiationIncomeA = incomeA ** 0.9;
exponetiationIncomeB = incomeB ** 0.9;

ExponentiatedDifference =
  Math.max(exponetiationIncomeA, exponetiationIncomeB) -
  Math.min(exponetiationIncomeA, exponetiationIncomeB);

console.log(
  "The income difference after exponentiation =",
  ExponentiatedDifference
);
