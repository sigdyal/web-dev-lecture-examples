/* Assignment 6. (Area of a square)

Create a variable for the length of a side in a square.
Calculate and print out the area of a square with the given length of each side.
For example, a square with sides that are 5 meters long is 25 square meters in area.
EXTRA: Calculate the area with expnonentiation ** instead of multiplication *.
*/

let sideLength = 5;

AreaOfSquare1 = sideLength * sideLength;
AreaOfSquare2 = sideLength ** 2; //with exponentiation

console.log(
  "The area of square with sidelength =",
  sideLength,
  "is:",
  AreaOfSquare1
);
console.log(
  "The area of square with sidelength =",
  sideLength,
  "is:",
  AreaOfSquare2
);
