/*Assignment 5 (Average grade)
Your friend is making a program for calculating their average grade. 
However, it doesn't give the correct result, and they're not sure why.
Fix their code so that the average grade is calculated correctly.
*/

const grade1 = 8;
const grade2 = 10;
const grade3 = 7;

const gradeCount = 3;

const totalGrades = grade1 + grade2 + grade3;

const averageGrade = totalGrades / gradeCount;

console.log("The average grade is", averageGrade);
