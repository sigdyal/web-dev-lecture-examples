/*Assignment 4 (Seconds in a year)
Calculate how many seconds there are in a year. 
Use variables for days, hours, minutes and seconds in a year. Print out the result.
*/

const seconds = 60;
const minute = 60;
const hours = 24;
const days = 365;

const secondsInYear = days * hours * minute * seconds;
console.log("There are", secondsInYear, "seconds in 1 Year.");
