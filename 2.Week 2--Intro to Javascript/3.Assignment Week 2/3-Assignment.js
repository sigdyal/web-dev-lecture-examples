/*
Assigngment 3.

Create variables for distance (kilometers) and speed (km/h), and give them some values. 
Calculate and print out how many hours it takes to travel the distance at the given speed.
EXTRA: Express the time in hours and minutes instead of only hours. 
For example, traveling 120 km at 50 km/h would take 2 hours 24 minutes.

*/

let distance = 600;
let speed = 120;

timeTaken = distance / speed;
console.log(timeTaken, "hours");

hour = Math.floor(timeTaken);
minutes = distance % speed;

console.log(
  "The distance of",
  distance,
  "kilometers can be completed in",
  hour,
  "hours and",
  minutes,
  "minutes at a speed of",
  speed,
  " kmph"
);
