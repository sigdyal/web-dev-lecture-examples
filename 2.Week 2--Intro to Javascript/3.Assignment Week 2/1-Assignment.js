/*
Assignment 1. The following code doesn't work.
Figure out why it doesn't work, and fix it so that it works.

const value = 30;
const multiplier = 1.2;
value = value * multiplier;
console.log(value);

Answer: The code doesn't work as the variable 'value' is a const.
We make the code work again by assigning 'let' to the value variable.
*/

let value = 30;
const multiplier = 1.2;
value = value * multiplier;
console.log(value);
