const text = "";
const number1 = -12345;
const bool1 = true;
const null1 = null;
let undefined1;

if (bool1 === true) {
  console.log("bool equals true");
}

let changingData = "terve";
console.log("terve is", typeof changingData);

console.log("nummber1 is a", typeof number1);

changingData = true;
console.log("true is", typeof changingData);

changingData = null;
console.log("null is", typeof changingData);

changingData = NaN;
console.log("NaN is", typeof changingData);

changingData = undefined;
console.log("undefined is", typeof changingData);

console.log(typeof text);
