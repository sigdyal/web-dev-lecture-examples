/*
const shapes = [
  "circle",
  "triangle",
  "squircle",
  "circle",
  "dodecahedron",
  234,
  1,
  12345,
  true,
  undefined,
  "oh no",
];

shapes[shapes.length - 2] = "pentagon";

console.log(shapes[0]); //first element
console.log(shapes[shapes.length - 2]); //second to last element
console.log(shapes[0][0]); // first character of the first element (a string)
*/

const fruits = [
  "Apple",
  "Banana",
  "Pineapple",
  "Mango",
  "Peach",
  "Strawberry",
  "Blueberry",
];

console.log(fruits);
console.log(fruits.length);
fruits[fruits[1]] = "Kiwi";
console.log(fruits);

fruits.push("Orange");
console.log(fruits);

console.log(fruits.length);

fruits.pop();
console.log(fruits);

console.log(fruits);

const fruits2 = ["fasdfs", "fdgfdg", "fdsfdsfdg"];
console.log(fruits2);
fruits2[fruits2[1]] = "apple";
console.log(fruits2);
console.log(typeof fruits2);
console.log(fruits2[1]);
