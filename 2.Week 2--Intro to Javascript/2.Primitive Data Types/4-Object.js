const obj = {
  avain1: "arvo1",
  avain2: 25,
  avain3: true,
  avain4: undefined,
  other_obj: {
    x: 14,
    y: "name",
    z: 12321,
  },
};

console.log(obj.avain1);
console.log(obj.other_obj.z);

const playerCharacter = {
  position: {
    x: 123,
    y: 234,
  },
  name: "Player",
  canJump: true,
};
playerCharacter.position.x += 1;
playerCharacter.name = "Matti";

console.log(playerCharacter.name);

const person = {
  name: "John",
  age: 32,
  food: {
    food1: "pizza",
    likesPineapple: false,
  },
};

console.log(person);
person.food2 = "Noodles";
console.log(person);

delete person.food2;
console.log(person);

person.food.food2 = "Noodles";
console.log(person);

typeof person;
console.log(typeof perso1);

person.name = "Smith";
person.age = 48;
(person.food.food1 = "rice"), (person.food.likesPineapple = true);
person.food.food2 = "Pizza";

console.log(person);
