const myName = "Matti";
const char = myName[1];
console.log(char);

const name1 = "John";
const name2 = "Smith";
const name3 = `Jane`;
console.log(name1, name2, name3);

const character1 = name1[0];
const character2 = name1[1];
const character3 = name1[2];
const character4 = name1[3];

console.log(character1);
console.log(character2);
console.log(character3);
console.log(character4);
console.log(name1.length);
console.log(myName[myName.length - 1]);
