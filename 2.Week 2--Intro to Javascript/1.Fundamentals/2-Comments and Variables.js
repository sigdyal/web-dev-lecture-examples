let teksti = "exercise 1 is done!";
const text2 = "Exercise 1 is done!";
console.log(text2);
console.log(teksti);

// This line is a comment
/*This is a 
multiline comment */

let a = 1;
const b = 2;
let c;
c = a + b;
console.log(c);

const firstName = "John";
let text = " Smith";
console.log(firstName + text);

let vaiableOne = 5;
let variableTwo = 7;
console.log(vaiableOne, variableTwo);
