const a = 10;
const b = 5;
const c = 2;

console.log("The sum of the numbers is ", a + b);
console.log("Difference is ", a - b);
console.log("Product is ", a * b);
console.log("Fraction is ", a / b);

console.log("a + b*c is ", a + b * c);
console.log("(a + b)*c is ", (a + b) * c);
console.log("a + (b*c) is ", a + b * c);

let i = 5;
const j = i++;
const k = ++i;
console.log(i, j, k);

let w = 6;
let x = 7;
let y = 8;
let z = 9;

w += 1; // w = w + 1
x -= 2; // x = x - 1
y *= 3; // y = y * 3
z /= 4; // z = z /4
console.log(w, x, y, z);
