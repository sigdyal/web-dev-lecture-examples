//Create a program that calculates the result for them: have two variables, price and increase. Based on these, calculate the final price into a variable named result.
//Print the values of all of these variables on their own lines, with fitting explanations. For example, "Original price: 6.5".

const price = 50;
const increase = 10;

result = price + increase;

console.log("Original price =", price);
console.log("Increase in price =", increase);
console.log("New price =", price, "+", increase, "=", result);
