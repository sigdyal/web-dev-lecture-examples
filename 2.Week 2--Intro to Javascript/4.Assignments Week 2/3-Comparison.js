const person1Age = 15;
const person2Age = 24;

const isFirstPersonOlder = person1Age > person2Age;
console.log(isFirstPersonOlder);
console.log(typeof isFirstPersonOlder);

/*

a) The code will give a boolean value output as the variable 'isFirstPersonOlder'. In this
case, the result will be false as the condition in line 4 is false.


b) The type of 'isFirstPersonOlder' is a boolean which results in either true or false.

*/

constA = [9, 6, 9];
constB = [7, 10, 5];

averageA = (constA[0] + constA[1] + constA[2]) / constA.length;
averageB = (constB[0] + constB[1] + constB[2]) / constB.length;

const isHigherThanA = averageB > averageA;

console.log("Average of A:", averageA);
console.log("Average of B:", averageB);
console.log("B is highger than A: ", isHigherThanA);
