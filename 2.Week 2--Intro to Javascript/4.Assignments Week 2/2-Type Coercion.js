let number;
console.log(number); //Before being assigned a null, the variable 'number' is undefined.

const result1 = 10 + number;

number = null;
const result2 = 10 + number;

console.log(result1); // The result is Not a Number (NaN).
console.log(result2); // The result is a number 10.

console.log("********************************************************");

const a = true;
const b = false;

const c = a + b;
const d = 10 + a;
const e = 10 + b;

console.log(c);
//The value of c is 1 because the boolean value of 'a' is true which gives it a value of 1.

console.log(d);
//The value of d is 11 because the boolean value of 'a' (true) is treated as 1 and added to 10 to give 11.

console.log(e);
//The value of e is 10 because the boolean value of 'b' is false which is converted to 0 and added to 10 to give 10.
