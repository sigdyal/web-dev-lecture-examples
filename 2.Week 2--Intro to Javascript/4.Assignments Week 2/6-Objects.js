//Creating an Object
book1 = { name: "Dune", pageCount: 412, isRead: true };
book2 = { name: "The Eye of the World", pageCount: 782, isRead: false };

console.log(book1);
console.log(book2);

//editing the 'isRead' variable case
book1.isRead = false;
book2.isRead = true;

console.log(book1);
console.log(book2);

//Extra

Array1 = [(name = "Dune"), (pageCount = 412), (isRead = true)];
Array2 = [(name = "The Eye of the World"), (pageCount = 782), (isRead = false)];

console.log(Array1);
console.log(Array2);
